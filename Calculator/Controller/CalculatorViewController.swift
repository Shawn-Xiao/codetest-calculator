import UIKit

/**
 A view controller that manages the display and user interactions of a calculator
 
 - Author:
 Wenda(Shawn) Xiao
 
 */

class CalculatorViewController: UIViewController {
    
    private var calculator = Calculator()
    
    
    /// a indicator of whether someone is typing operand
    private var isInTheMiddleOfTypingOperand = false
    
    
    /// True if calculator displays 0
    /// - Note:
    /// When the calculator displays 0, special conditions needs to be considered
    private var isCalculatorDisplayingZero: Bool {
        get {
            return calculatorDisplay.text! == "0"
        }
    }
    
    
    /// sync with calculatorDisplay
    private var displayedValue: Double {
        get {
            return getValueFromCalculatorDisplay()
        }
        set {
            calculatorDisplay.text = String(format: "%g", newValue)
        }
        
    }
    
    
    @IBOutlet weak var calculatorDisplay: UILabel!
    
    
    /// This IBAction is wired up with digit number (0-9 ten buttons)
    ///
    /// - Note:
    /// * If the user is in the middle of typing a new operand or the calculator displays 0
    /// set the new operand as the display value,
    /// * Otherwise append it at the end of the current displayed text
    @IBAction func numTouched(_ sender: UIButton) {
        let digit = sender.currentTitle!
        if !isInTheMiddleOfTypingOperand || isCalculatorDisplayingZero {
            calculatorDisplay.text = digit
            isInTheMiddleOfTypingOperand = true
        } else {
            
            calculatorDisplay.text = calculatorDisplay.text! + digit
        }
    }
    
    
    /// This IBAction is wired up with AC button
    /// It will reset the calculator entirely (both the display and the pending operation)
    @IBAction func allClearButtonTouched(_ sender: UIButton) {
        clearCalculatorDisplay()
        calculator.clear()
    }
    
    /// This IBAction is wired up with "C" Button
    @IBAction func clearButtonTouched(_ sender: UIButton) {
        clearCalculatorDisplay()
    }
    
    
    // This IBAction is wired up with all operation buttons
    @IBAction func calculationSymbolTouched(_ sender: UIButton) {
        if isInTheMiddleOfTypingOperand || isCalculatorDisplayingZero {
            calculator.typeNewOperand(getValueFromCalculatorDisplay())
        }
        
        if let touchedSymbol = sender.currentTitle {
            calculator.calculate(by: touchedSymbol)
        }
        
        if let calculatedResult = calculator.result {
            displayedValue = calculatedResult
        }
        
        isInTheMiddleOfTypingOperand = false
        
    }
    
    /// This IBAction is wired up with "."
    @IBAction func dotTouched(_ sender: UIButton) {
        if !isInTheMiddleOfTypingOperand || isCalculatorDisplayingZero {
            calculatorDisplay.text = "0."
        } else {
            if let currentCalculatorDisplay = calculatorDisplay.text {
                if !currentCalculatorDisplay.contains(".") {
                    calculatorDisplay.text =  currentCalculatorDisplay + "."
                }
            }
        }
        isInTheMiddleOfTypingOperand = true
    }
}


/**
 a extention that holds the helper functions for clearer structure
 
 - Author:
 Wenda(Shawn) Xiao
 
 */

extension CalculatorViewController {
    
    /**
     A helper function for resetting the calcultor display to initial state
     
     After calling this function, the calculator will display "0" and assert no one is currently typing operands
     
     - Important:
     This helper function will not clear the pending operation in calculator
     
     */
    private func clearCalculatorDisplay() {
        isInTheMiddleOfTypingOperand = false
        displayedValue = 0
    }
    
    
    /**
     A helper function for parsing the displayed calculator String value to Double
     
     - Important:
     The condition that user is just typing "." at the end has been considered
     
     - returns:
     Calculator displayed value in Double
     
     If no text is displaying, return 0
     
     */
    private func getValueFromCalculatorDisplay() -> Double{
        if var currentCalculatorDisplay = calculatorDisplay.text {
            if let lastDigit = currentCalculatorDisplay.last, lastDigit == "." {
                currentCalculatorDisplay.removeLast()
            }
            return Double(currentCalculatorDisplay)!
        }
        return 0
    }
}

