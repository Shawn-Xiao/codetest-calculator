import Foundation

/**
 Calculator brain
 
 - Important:
 Implemented Operations:
 * Mathmatical Constant: e, π
 * Unary Operations: cos, sin, +/-(change sign), √
 * Binary Operations: +, -, *, /
 * Equal: =
 
 - Author:
 Wenda(Shawn) Xiao
 
 */
struct Calculator {
    
    /// a enum of all types of operations
    private enum Operation {
        case mathContant(Double)
        case unaryOperation((Double) -> Double)
        case binaryOperation((Double,Double) -> Double)
        case equal
    }
    
    
    /// a collection of mapping between implemented operations and symbols
    private var operations = [
        "e" : Operation.mathContant(M_E),
        "π" : Operation.mathContant(Double.pi),
        "cos" : Operation.unaryOperation(cos),
        "sin" : Operation.unaryOperation(sin),
        "+/-" : Operation.unaryOperation(CalculatorOperations.changeSign),
        "√" : Operation.unaryOperation(sqrt),
        "+" : Operation.binaryOperation(CalculatorOperations.plus),
        "-" : Operation.binaryOperation(CalculatorOperations.minus),
        "*" : Operation.binaryOperation(CalculatorOperations.multiply),
        "/" : Operation.binaryOperation(CalculatorOperations.divide),
        "=" : Operation.equal
    ]
    
    /// This optional is used to store the pending binary operation
    private var pendingBinaryOperation: ((Double, Double) -> Double)?
    
    /// This optional is used to store the first operand of pending binary operaion
    private var pendingBinaryOperationOperand: Double?
    
    private var accumulator: Double?
    
    var result: Double? {
        get {
            return accumulator
        }
    }
    
    
    /**
     Update user typed operand to the accumulator of calculator
     - parameters:
          - operand: the number user typed in the calculator.
     */
    mutating func typeNewOperand(_ operand: Double){
            accumulator = operand
    }
    
    
    /**
     Perform actual calculating operations
     - parameters:
        - symbol: the operation symbol user pressed
     */
    mutating func calculate(by symbol: String) {
        if let theOperation = operations[symbol] {
            switch theOperation {
            case .mathContant(let constant):
                accumulator = constant
            case .unaryOperation(let unaryOperation):
                if accumulator != nil {
                    accumulator = unaryOperation(accumulator!)
                }
            case .binaryOperation(let binaryOperation):
                if accumulator != nil {
                    if pendingBinaryOperation != nil && pendingBinaryOperationOperand != nil {
                        accumulator = pendingBinaryOperation!(pendingBinaryOperationOperand!, accumulator!)
                    }
                    pendingBinaryOperation = binaryOperation
                    pendingBinaryOperationOperand = accumulator!
                }
            case .equal:
                if pendingBinaryOperationOperand != nil && pendingBinaryOperation != nil && accumulator != nil {
                    accumulator = pendingBinaryOperation!(pendingBinaryOperationOperand!, accumulator!)
                    pendingBinaryOperationOperand = nil
                    pendingBinaryOperation = nil
                }
            }
        }
    }
    
    /// Reset calculator
    mutating func clear() {
        accumulator = nil
        pendingBinaryOperationOperand = nil
        pendingBinaryOperation = nil
    }
    
}

