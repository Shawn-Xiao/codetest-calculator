import Foundation

/**
 A list of calculation methods
 
 - Author:
 Wenda(Shawn) Xiao
 
 - Note:
 Implemented calculation operations:
 * change sign
 * addition
 * substraction
 * multiplication
 * division
 
 - Important:
 These methods are static
 */

struct CalculatorOperations {
    
    /// Change the sign of operand
    
    static func changeSign(operand: Double) -> Double {
        return -operand
    }
    
    
    /// Perform plus operation of two operands
    /// - parameters:
    ///     -lhs: left-hand side operand
    ///     -rhs: right-hand side operand
    
    static func plus(lhs: Double, rhs: Double) -> Double {
        return lhs + rhs
    }
    
    
    /// Perform minus operation of two operands
    /// - parameters:
    ///     -lhs: left-hand side operand
    ///     -rhs: right-hand side operand
    
    static func minus(lhs: Double, rhs: Double) -> Double {
        return lhs - rhs
    }
    
    
    /// Perform multiplication operation of two operands
    /// - parameters:
    ///     -lhs: left-hand side operand
    ///     -rhs: right-hand side operand
    
    static func multiply(lhs: Double, rhs: Double) -> Double {
        return lhs * rhs
    }
    
    
    /// Perform division operation of two operands
    /// - parameters:
    ///     -lhs: left-hand side operand
    ///     -rhs: right-hand side operand
    
    static func divide(lhs: Double, rhs: Double) -> Double{
        return lhs / rhs
    }
}
