## Calculator Project Brief Outline

 1. Implemented Operations
    * Math Constants -- "π", "e"
    * Unary Operations -- "cos", "sin", "√", "+/-"
    * Binary Operations -- "+", "-", "*", "/"
    * Clear Operaions -- "AC", "C"
    
 2. Auto layout for diffrent devices and orientations
    * Show more operations in landscape mode
    * Adjust font size in different devices to achieve better UX 
    
 3. The Calculator can deal with decimals
 
## Environment

   Xcode 10.1, iOS 12.1

## Time spent

 * Due to the request to hand in the code test in one to two days by Amy, I spent about 12 hours on this test

## Limitations

Due to the limited time, there are still some limitations on this calculator:

 * User can perform "√" operation on negative number or division on zero (display nah not error)
 * Input super large number may result in trunctuation
